import os
import json as j

class PXE():

  def __init__(self, pw=None, interface=None, range_ips=None, local_ip=None, gateway=None, mask=None, centos_dir=None):
    self.pw = pw
    self.interface = interface
    self.range = range_ips
    self.local_ip = local_ip
    self.mask = mask
    self.gateway = gateway
    self.centos_dir = centos_dir

    if self.centos_dir is not None:
      self.centos_dir_flag = True
    else:
      self.centos_dir_flag = False

    self.abspath = os.path.abspath(os.path.dirname(__file__))

  def set_pw(self, pw):
    self.pw = pw

  def set_interface(self, interface):
    self.interface = interface

  def set_range_ips(self, range_ips):
    self.range = range_ips

  def set_local_ip(self, local_ip):
    self.local_ip = local_ip

  def set_gateway(self, gateway):
    self.gateway = gateway

  def set_mask(self, mask):
    self.mask = mask

  def set_centos_dir(self, centos_dir):
    self.centos_dir = centos_dir

  def read_json(self):
    json_dir = os.path.join(self.abspath, 'config.json')
    f = open(json_dir,)
    data = j.load(f)

    self.set_pw(data["pw"])
    self.set_interface(data["interface"])
    self.set_range_ips(data["range_ips"])
    self.set_local_ip(data["local_ip"])
    self.set_gateway(data["gateway"])
    self.set_mask(data["mask"])

    try:
      self.set_centos_dir(data["centos_dir"])
      self.centos_dir_flag = True
    except:
      self.centos_dir_flag = False
      print('No centOS mounted dir!')


  def sudo_command(self, command):
    strg = f'echo "{self.pw}" | sudo -S -k {command}'
    os.system(strg)

  def simple_command(self, command):
    os.system(command)

  def static_ip(self):

    static_ip_dir = '/etc/sysconfig/network-scripts'
    local_interface = 'ifcfg-' + self.interface
    static_ip_dir = os.path.join(static_ip_dir,local_interface)

    file = open(static_ip_dir,'r')
    list_of_lines = file.readlines()

    list_of_lines[3] = 'BOOTPROTO=none\n'

    file = open(static_ip_dir,"w")
    file.writelines(list_of_lines)
    file.close()

    text = f'IPADDR={self.local_ip}\nPREFIX=24\nGATEWAY={self.gateway}\nDNS1={self.gateway}\nDNS2=8.8.8.8\nDNS3=8.8.4.4\nPEERDNS=no'

    file = open(static_ip_dir,'a')
    file.write(text)
    file.close()


  def install_stuff(self):
    self.sudo_command('dnf makecache')
    self.sudo_command('dnf install -y dnsmasq syslinux httpd')

  def disable_selinux(self):

    self.sudo_command('setenforce 0')
    selinux_dir = '/etc/selinux/config'
    # dir = os.path.join(selinux_dir, 'config')
    file = open(selinux_dir,'r')
    list_of_lines = file.readlines()
    list_of_lines[6] = 'SELINUX=permissive\n'

    file = open(selinux_dir,'w')
    file.writelines(list_of_lines)
    file.close()

    self.sudo_command('sestatus')

    # myText = open(dir,'w')
    # myString = 'Type your string here\ndklsahdljhas'
    # myText.write(myString)
    # myText.close()

  def disable_firewall(self):

    self.sudo_command('systemctl stop firewalld')
    self.sudo_command('systemctl disable firewalld')
    self.sudo_command('systemctl status firewalld --no-pager')

  def dnsmasq_config(self):

    ips = self.range.split(',')
    ip_low = ips[0]
    ip_up = ips[1]

    text = f'interface={self.interface}\nbind-interfaces\ndomain=linuxhint.local\ndhcp-range={self.interface},{ip_low},{ip_up},{self.mask},8h\ndhcp-option=option:router,{self.gateway}\ndhcp-option=option:dns-server,{self.gateway}\ndhcp-option=option:dns-server,8.8.8.8\nenable-tftp\ntftp-root=/netboot/tftp\ndhcp-boot=pxelinux.0,linuxhint-s80,{self.local_ip}\npxe-prompt="Press F8 for PXE Network boot.",30\npxe-service=x86PC,"Install OS via PXE",pxelinux'

    if not os.path.isfile('/etc/dnsmasq.conf.backup'):
      self.sudo_command('mv -v /etc/dnsmasq.conf /etc/dnsmasq.conf.backup')

    dir_dnsmasq_config = '/etc/dnsmasq.conf'

    touch_cmd = f'touch {dir_dnsmasq_config}'

    self.sudo_command(touch_cmd)

    file = open(dir_dnsmasq_config,'w')
    file.write(text)
    file.close()

    self.sudo_command('mkdir -pv /netboot/tftp/pxelinux.cfg')
    self.sudo_command('systemctl restart dnsmasq')
    self.sudo_command('systemctl status dnsmasq --no-pager')
    self.sudo_command('systemctl enable dnsmasq')

  def syslinux_config(self):

    self.sudo_command('cp -v /usr/share/syslinux/{pxelinux.0,menu.c32,ldlinux.c32,libutil.c32} /netboot/tftp/')

  def apache_config(self):

    self.sudo_command('ln -s /var/www/html /netboot/www')
    self.sudo_command('systemctl start httpd')
    self.sudo_command('systemctl status httpd --no-pager')
    self.sudo_command('systemctl enable dnsmasq')

  def install_centos(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue: ')

      if inp == 'r' or inp == 'ready':
        ready = True

    if self.centos_dir_flag:
      self.sudo_command('mkdir -v /netboot/{tftp,www}/centos8')

      strg = ''

      if self.centos_dir[0] == '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz {self.centos_dir} /netboot/www/centos8'
      elif self.centos_dir[0] == '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz {self.centos_dir}/ /netboot/www/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz /{self.centos_dir} /netboot/www/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz /{self.centos_dir}/ /netboot/www/centos8'

      self.sudo_command(strg)
      self.sudo_command('cp -v /netboot/www/centos8/images/pxeboot/{initrd.img,vmlinuz} /netboot/tftp/centos8/')

  def install_centos_menu(self):

    default_menu_dir = '/netboot/tftp/pxelinux.cfg/default'

    if not os.path.isfile(default_menu_dir):
      touch_default_menu = f'touch {default_menu_dir}'
      self.sudo_command(touch_default_menu)

    file = open(default_menu_dir,'r')
    lines = file.readlines()
    file.close()
    default_text = 'default menu.c32\nprompt 0\ntimeout 300\nONTIMEOUT local\n\nmenu title PXE Boot Menu\n\n'

    label_number = 0
    label_txt = 'label 1\n'

    if len(lines)==0:
      file = open(default_menu_dir,"w")
      file.write(default_text)
      file.close()
    else:
      for line in lines:
        if line == label_txt:
          label_number=label_number+1
          label_txt = label_txt[:-3] + str(label_number+1) + str('\n')

    centos_text = f'label {label_number+1}\nmenu label ^{label_number+1}) Install CentOS8\nkernel centos8/vmlinuz\nappend initrd=centos8/initrd.img ip=dhcp inst.repo=http://{self.local_ip}/centos8/\n'

    file = open(default_menu_dir,'a')
    file.write(centos_text)
    file.close()

  def run_config(self):

    self.static_ip()
    self.install_stuff()
    self.disable_selinux()
    self.disable_firewall()
    self.dnsmasq_config()
    self.syslinux_config()
    self.apache_config()

  def run_centos(self):

    self.run_config()
    self.install_centos()
    self.install_centos_menu()


if __name__ == "__main__":

  pxe = PXE()
  pxe.read_json()

  decision = False
  centos_flag = False

  while not decision:
    op = input('Install centos?[y/n]: ')

    if op=='y' or op=='n':
      decision = True

      if op == 'y':
        centos_flag = True

  if not centos_flag:
    pxe.run_config()
  else:
    pxe.run_centos()
