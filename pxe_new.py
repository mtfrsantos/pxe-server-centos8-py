import os
import json as j

class PXE():

  def __init__(self, interface=None, range_ips=None, local_ip=None, gateway=None, mask=None, centos_dir=None, ubuntu_dir=None, windows_dir=None):
    self.interface = interface
    self.range = range_ips
    self.local_ip = local_ip
    self.mask = mask
    self.gateway = gateway
    self.centos_dir = centos_dir
    self.ubuntu_dir = ubuntu_dir
    self.windows_dir = windows_dir

    # CentOS dir
    if self.centos_dir is not None:
      self.centos_dir_flag = True
    else:
      self.centos_dir_flag = False

    # Ubuntu dir
    if self.ubuntu_dir is not None:
      self.ubuntu_dir_flag = True
    else:
      self.ubuntu_dir_flag = False

    # Windows dir
    if self.windows_dir is not None:
      self.windows_dir_flag = True
    else:
      self.windows_dir_flag = False


    self.abspath = os.path.abspath(os.path.dirname(__file__))

  def set_interface(self, interface):
    self.interface = interface

  def set_range_ips(self, range_ips):
    self.range = range_ips

  def set_local_ip(self, local_ip):
    self.local_ip = local_ip

  def set_gateway(self, gateway):
    self.gateway = gateway

  def set_mask(self, mask):
    self.mask = mask

  def set_centos_dir(self, centos_dir):
    self.centos_dir = centos_dir

  def set_ubuntu_dir(self, ubuntu_dir):
    self.ubuntu_dir = ubuntu_dir

  def set_windows_dir(self, windows_dir):
    self.windows_dir = windows_dir

  def command(self, command):
    os.system(command)

  def read_json(self):
    json_dir = os.path.join(self.abspath, 'config.json')
    f = open(json_dir,)
    data = j.load(f)

    self.set_interface(data["interface"])
    self.set_range_ips(data["range_ips"])
    self.set_local_ip(data["local_ip"])
    self.set_gateway(data["gateway"])
    self.set_mask(data["mask"])

    try:
      self.set_centos_dir(data["centos_dir"])
      self.centos_dir_flag = True
    except:
      self.centos_dir_flag = False
      print('No centOS mounted dir!')

    try:
      self.set_ubuntu_dir(data["ubuntu_dir"])
      self.ubuntu_dir_flag = True
    except:
      self.ubuntu_dir_flag = False
      print('No Ubuntu mounted dir!')

    try:
      self.set_windows_dir(data["windows_dir"])
      self.windows_dir_flag = True
    except:
      self.windows_dir_flag = False
      print('No Windows mounted dir!')

  def static_ip(self):

    static_ip_dir = '/etc/sysconfig/network-scripts'
    local_interface = 'ifcfg-' + self.interface
    static_ip_dir = os.path.join(static_ip_dir,local_interface)

    file = open(static_ip_dir,'r')
    list_of_lines = file.readlines()

    list_of_lines[3] = 'BOOTPROTO=none\n'

    file = open(static_ip_dir,"w")
    file.writelines(list_of_lines)
    file.close()

    text = f'IPADDR={self.local_ip}\nPREFIX=24\nGATEWAY={self.gateway}\nDNS1={self.gateway}\nDNS2=8.8.8.8\nDNS3=8.8.4.4\nPEERDNS=no'

    file = open(static_ip_dir,'a')
    file.write(text)
    file.close()

    print('\n-> Static IP UP!')

  def install_stuff(self):
    self.command('dnf makecache')
    self.command('dnf install -y dnsmasq syslinux httpd samba')
    self.command('pip3 install gdown')

    print('\n-> Installation completed!')

  def disable_selinux(self):

    self.command('setenforce 0')
    selinux_dir = '/etc/selinux/config'
    file = open(selinux_dir,'r')
    list_of_lines = file.readlines()
    list_of_lines[6] = 'SELINUX=permissive\n'

    file = open(selinux_dir,'w')
    file.writelines(list_of_lines)
    file.close()

    self.command('sestatus')

    print('\n-> SELinux disabled!')

  def disable_firewall(self):

    self.command('systemctl stop firewalld')
    self.command('systemctl disable firewalld')
    self.command('systemctl status firewalld --no-pager')

    print('\n-> Firewall disabled!')

  def dirmaker(self):

    self.command('mkdir -p /tftp/{boot,grub}')
    self.command('mkdir -p /var/www/html/{desktop,server}')
    self.command('mkdir /var/www/html/server/centos8')
    self.command('mkdir /tftp/boot/{windows10,centos8,focal}')
    self.command('mkdir /smbshare')
    self.command('mkdir /nfsshare')
    self.command('mkdir -p /netboot/files')
    self.command('mkdir /tftp/pxelinux.cfg')

    print('\n-> Created directories!')

  def nfs_server_config(self):
    self.command('systemctl start nfs-server')
    self.command('systemctl enable nfs-server')
    self.command('systemctl status nfs-server --no-page')
    self.command('chown nobody:nobody /nfsshare')
    self.command('echo "/nfsshare *(ro)" >> /etc/exports')
    self.command('exportfs -r')

    print('\n--> Nfs-server configured!')

  def samba_config(self):

    self.command('useradd -s /sbin/nologin win10')
    self.command('echo -ne "123\n123\n" | smbpasswd -a -s win10')

    text = '\n[install]\n         comment = Installation Media\n         path = /smbshare\n         public = yes\n         writable = yes\n         printable = no\n         browseable = yes\n'

    dir_smb_config = '/etc/samba/smb.conf'

    file = open(dir_smb_config, 'a')
    file.write(text)
    file.close()

    self.command('systemctl start smb nmb')
    self.command('systemctl enable smb nmb')
    self.command('systemctl status smb nmb --no-page')

    print('\n--> Samba configured!')

  def dnsmasq_config(self):

    ips = self.range.split(',')
    ip_low = ips[0]
    ip_up = ips[1]

    text = f'interface={self.interface}\nbind-interfaces\ndomain=linuxhint.local\ndhcp-range={self.interface},{ip_low},{ip_up},{self.mask},8h\ndhcp-option=option:router,{self.gateway}\ndhcp-option=option:dns-server,{self.gateway}\ndhcp-option=option:dns-server,8.8.8.8\nenable-tftp\ntftp-root=/tftp\ndhcp-boot=pxelinux.0,linuxhint-s80,{self.local_ip}\npxe-prompt="Press F8 for PXE Network boot.",30\npxe-service=x86PC,"Install OS via PXE",pxelinux'

    if not os.path.isfile('/etc/dnsmasq.conf.backup'):
      self.command('mv -v /etc/dnsmasq.conf /etc/dnsmasq.conf.backup')

    dir_dnsmasq_config = '/etc/dnsmasq.conf'

    touch_cmd = f'touch {dir_dnsmasq_config}'

    self.command(touch_cmd)

    file = open(dir_dnsmasq_config,'w')
    file.write(text)
    file.close()

    self.command('systemctl restart dnsmasq')
    self.command('systemctl status dnsmasq --no-pager')
    self.command('systemctl enable dnsmasq')

    print('\n-> Dnsmasq configured!')

  def apache_config(self):

    self.command('systemctl start httpd')
    self.command('systemctl status httpd --no-pager')
    self.command('systemctl enable http')

    print('\n-> Apache server configured!')

  def install_centos(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing centos: ')

      if inp == 'r' or inp == 'ready':
        ready = True

    if self.centos_dir_flag:

      strg = ''

      if self.centos_dir[0] == '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz {self.centos_dir} /var/www/html/server/centos8'
      elif self.centos_dir[0] == '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz {self.centos_dir}/ /var/www/html/server/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz /{self.centos_dir} /var/www/html/server/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz /{self.centos_dir}/ /var/www/html/server/centos8'

      self.command(strg)
      self.command('cp -v /var/www/html/server/centos8/images/pxeboot/{initrd.img,vmlinuz} /tftp/boot/centos8/')

    print('\n-> CentOS installed!')

  def install_windows(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing windows: ')

      if inp == 'r' or inp == 'ready':
        ready = True

    if self.windows_dir_flag:

      strg = ''

      if self.windows_dir[0] == '/' and self.windows_dir[-1] == '/':
        strg = f'rsync -avz {self.windows_dir} /smbshare/windows10'
      elif self.windows_dir[0] == '/' and self.windows_dir[-1] != '/':
        strg = f'rsync -avz {self.windows_dir}/ /smbshare/windows10'
      elif self.windows_dir[0] != '/' and self.windows_dir[-1] == '/':
        strg = f'rsync -avz /{self.windows_dir} /smbshare/windows10'
      elif self.windows_dir[0] != '/' and self.windows_dir[-1] != '/':
        strg = f'rsync -avz /{self.windows_dir}/ /smbshare/windows10'

      self.command(strg)
      self.command('chown -R win10:win10 /smbshare/')
      self.command('chmod 777 -R /smbshare')
      self.command('cd /tftp/boot/windows10')
      self.command('gdown --id 1a1T7bSLXnB-yL1y7VvCzoPSANZKvW0lD')
      self.command('cd')

      print('\n-> Windows installed!')

  def install_ubuntu(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing ubuntu: ')

      if inp == 'r' or inp == 'ready':
        ready = True

    if self.ubuntu_dir_flag:

      strg = ''

      if self.ubuntu_dir[0] == '/' and self.ubuntu_dir[-1] == '/':
        strg = f'rsync -avz {self.ubuntu_dir} /nfsshare/focal'
      elif self.ubuntu_dir[0] == '/' and self.ubuntu_dir[-1] != '/':
        strg = f'rsync -avz {self.ubuntu_dir}/ /nfsshare/focal'
      elif self.ubuntu_dir[0] != '/' and self.ubuntu_dir[-1] == '/':
        strg = f'rsync -avz /{self.ubuntu_dir} /nfsshare/focal'
      elif self.ubuntu_dir[0] != '/' and self.ubuntu_dir[-1] != '/':
        strg = f'rsync -avz /{self.ubuntu_dir}/ /nfsshare/focal'

      self.command(strg)

      text = '# Enable extras.ubuntu.com.\nd-i     apt-setup/extras        boolean true\n# Install the Ubuntu desktop.\ntasksel tasksel/first   multiselect ubuntu-desktop\n# On live DVDs, dont spend huge amounts of time removing substantial\n# application packages pulled in by language packs. Given that we clearly\n# have the space to include them on the DVD, theyre useful and we might as\n# well keep them installed.\nubiquity        ubiquity/keep-installed string icedtea6-plugin openoffice.org\n#System language\nlang en_US\n#Language modules to install\nlangsupport en_US\n#System keyboard\nkeyboard us\n#System mouse\nmouse\n#System timezone\ntimezone America/Sao_Paulo\n#Root password\nrootpw --disabled\n#Initial user (user with sudo capabilities)\nuser mtfrsantos --fullname "test" --password 1234\n#Reboot after installation\nreboot\n#Use text mode install\ntext\n#Install OS instead of upgrade\ninstall\n#Installation media\nnfs --server=192.168.100.119 --dir=/nfsshare/focal/\n#System bootloader configuration\nbootloader --location=mbr\n#Clear the Master Boot Record\nzerombr yes\n#Partition clearing information\nclearpart --all --initlabel\n#Basic disk partition\npart / --fstype ext4 --size 1 --grow --asprimary\npart swap --size 1024\npart /boot --fstype ext4 --size 256 --asprimary\n#System authorization infomation\nauth  --useshadow  --enablemd5\n#Network information\nnetwork --bootproto=dhcp --device=ens33\n#Firewall configuration\nfirewall --disabled --trust=ens33 --ssh\n%packages\nubuntu-desktop'

      ubuntu_ks_dir = '/nfsshare/focal/preseed/ubuntu.seed'

      # if not os.path.isfile(ubuntu_ks_dir):
      #   touch_ubuntu_ks_dir = f'touch {ubuntu_ks_dir}'
      #   self.command(touch_ubuntu_ks_dir)

      file = open(ubuntu_ks_dir, 'w')
      file.write(text)
      file.close()

      self.command('cd /netboot')
      self.command('wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/netboot.tar.gz')
      self.command('tar xvf netboot.tar.gz -C /netboot/files')
      self.command('cd')
      self.command('cp -v /netboot/files/ubuntu-installer/amd64/{linux,initrd.gz} /tftp/boot/focal/')

      print('\n-> Ubuntu installed!')

  def syslinux_config(self):

    self.command('cp -v /usr/share/syslinux/{pxelinux.0,memdisk,menu.c32,ldlinux.c32,libutil.c32,vesamenu.c32,lpxelinux.0,libcom32.c32} /tftp/')

    print('\n-> Syslinux configured!')

  def install_menu(self):

    default_menu_dir = '/tftp/pxelinux.cfg/default'

    if not os.path.isfile(default_menu_dir):
      touch_default_menu = f'touch {default_menu_dir}'
      self.command(touch_default_menu)

    text = 'DEFAULT vesamenu.c32\nMENU TITLE ULTIMATE PXE SERVER - By Griffon - Ver 2.0\nPROMPT 0\nTIMEOUT 0\n\nMENU COLOR TABMSG  37;40  #ffffffff #00000000\nMENU COLOR TITLE   37;40  #ffffffff #00000000\nMENU COLOR SEL      7     #ffffffff #00000000\nMENU COLOR UNSEL    37;40 #ffffffff #00000000\nMENU COLOR BORDER   37;40 #ffffffff #00000000\n\nLABEL Ubuntu 20\n    kernel /boot/focal/linux\n    initrd /boot/focal/initrd.gz\n    #append nfsroot=192.168.100.119:/nfsshare/focal netboot=nfs ip=dhcp ks=nfs:192.168.100.119:/nfsshare/focal/preseed/ubuntu.seed --- quiet\n    append netboot=nfs ip=dhcp ks=nfs:192.168.100.119:/nfsshare/focal/preseed/ubuntu.seed --- quiet\n\nLABEL CentOS 8\n    kernel /boot/centos8/vmlinuz\n    initrd /boot/centos8/initrd.img\n    append ip=dhcp inst.repo=http://192.168.100.119/server/centos8/\n\nLABEL Windows 10\n    kernel memdisk\n    initrd /tftp/boot/windows10/winpe.iso\n    append iso raw\n'

    file = open(default_menu_dir,'w')
    file.write(text)
    file.close()

    print('\n-> Menu configured!')

  def run(self):

    key = ''

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.static_ip()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.install_stuff()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.disable_selinux()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.disable_firewall()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.dirmaker()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.nfs_server_config()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.samba_config()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.dnsmasq_config()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.apache_config()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.install_centos()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.install_windows()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.install_ubuntu()

    while(key == ''):
      key = input('Press any key: ')
    key = ''

    self.syslinux_config()


if __name__ == "__main__":

  pxe = PXE()
  pxe.read_json()
  pxe.run()

