import getopt, sys
import os
import json as j

#! Problema (?) [def json_config_log(self)]: Se o log for executado posteriormente à execução da run() a variavél self.os_selected será igual ao que estiver no config.json mesmo que tenho sido alterado o os;
#! Problema [if __name__ == "__main__"]: O mesmo problema anterior se aplicado ao argumento --os/-o que também não é o mais atual;

# TODO: Tratar melhor o recebimento de sistemas operacionais e sistemas operacionais ativos no pxe menu;

class PXE():

  def __init__(self, interface=None, range_ips=None, local_ip=None, gateway=None, mask=None, centos_dir=None, ubuntu_dir=None, windows_dir=None, os_name='manual'):
    self.interface = interface
    self.range = range_ips
    self.local_ip = local_ip
    self.mask = mask
    self.gateway = gateway
    self.centos_dir = centos_dir
    self.ubuntu_dir = ubuntu_dir
    self.windows_dir = windows_dir

    if os_name == 'manual':
      self.os_selected = 0
    elif os_name == 'ubuntu':
      self.os_selected = 1
    elif os_name == 'centos':
      self.os_selected = 2
    elif os_name == 'windows':
      self.os_selected = 3
    else:
      self.os_selected = 0

    # CentOS dir
    if self.centos_dir is not None:
      self.centos_dir_flag = True
    else:
      self.centos_dir_flag = False

    # Ubuntu dir
    if self.ubuntu_dir is not None:
      self.ubuntu_dir_flag = True
    else:
      self.ubuntu_dir_flag = False

    # Windows dir
    if self.windows_dir is not None:
      self.windows_dir_flag = True
    else:
      self.windows_dir_flag = False

    self.abspath = os.path.abspath(os.path.dirname(__file__))

  def set_interface(self, interface):
    self.interface = interface

  def set_range_ips(self, range_ips):
    self.range = range_ips

  def set_local_ip(self, local_ip):
    self.local_ip = local_ip

  def set_gateway(self, gateway):
    self.gateway = gateway

  def set_mask(self, mask):
    self.mask = mask

  def set_centos_dir(self, centos_dir):
    self.centos_dir = centos_dir

  def set_ubuntu_dir(self, ubuntu_dir):
    self.ubuntu_dir = ubuntu_dir

  def set_windows_dir(self, windows_dir):
    self.windows_dir = windows_dir

  def select_os(self, os_name):

    if os_name == 'manual':
      self.os_selected = 0
    elif os_name == 'ubuntu':
      self.os_selected = 1
    elif os_name == 'centos':
      self.os_selected = 2
    elif os_name == 'windows':
      self.os_selected = 3
    else:
      self.os_selected = 0

  def command(self, command):
    os.system(command)

  def read_json(self):
    json_dir = os.path.join(self.abspath, 'config.json')
    f = open(json_dir,)
    data = j.load(f)

    self.set_interface(data["interface"])
    self.set_range_ips(data["range_ips"])
    self.set_local_ip(data["local_ip"])
    self.set_gateway(data["gateway"])
    self.set_mask(data["mask"])

    try:
      self.select_os(data["selected_os"])
    except:
      self.os_selected = 0

    try:
      self.set_centos_dir(data["centos_dir"])
      self.centos_dir_flag = True
    except:
      self.centos_dir_flag = False
      print('No centOS mounted dir!')

    try:
      self.set_ubuntu_dir(data["ubuntu_dir"])
      self.ubuntu_dir_flag = True
    except:
      self.ubuntu_dir_flag = False
      print('No Ubuntu mounted dir!')

    try:
      self.set_windows_dir(data["windows_dir"])
      self.windows_dir_flag = True
    except:
      self.windows_dir_flag = False
      print('No Windows mounted dir!')

  def json_config_log(self):

    cmd = '#################### LOG FILE ####################\n\n->Json config file read:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'interface: {self.interface}\nrange ips: {self.range}\nlocal ip: {self.local_ip}\ngateway: {self.gateway}\nmask: {self.mask}\ncentos dir: {self.centos_dir}\nubuntu dir: {self.ubuntu_dir}\nwindows dir: {self.windows_dir}\nselected os: {self.os_selected}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def static_ip(self):

    static_ip_dir = '/etc/sysconfig/network-scripts'
    local_interface = 'ifcfg-' + self.interface
    static_ip_dir = os.path.join(static_ip_dir,local_interface)

    file = open(static_ip_dir,'r')
    list_of_lines = file.readlines()

    list_of_lines[3] = 'BOOTPROTO=none\n'

    file = open(static_ip_dir,"w")
    file.writelines(list_of_lines)
    file.close()

    text = f'IPADDR={self.local_ip}\nPREFIX=24\nGATEWAY={self.gateway}\nDNS1={self.gateway}\nDNS2=8.8.8.8\nDNS3=8.8.4.4\nPEERDNS=no'

    file = open(static_ip_dir,'a')
    file.write(text)
    file.close()

    cmd1 = f'ifdown {self.interface}'
    cmd2 = f'ifup {self.interface}'
    cmd3 = f'ip add show {self.interface}'

    self.command(cmd1)
    self.command(cmd2)
    self.command(cmd3)

    print('\n-> Static IP UP!')

  def static_ip_log(self):

    static_ip_dir = f'/etc/sysconfig/network-scripts/ifcfg-{self.interface}'

    cmd = '-> Static IP:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {static_ip_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n\n' >> log.txt")
    cmd = f'ip add show {self.interface} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def install_stuff(self):
    self.command('dnf makecache')
    self.command('dnf install -y dnsmasq syslinux httpd samba')
    self.command('pip3 install gdown')

    print('\n-> Installation completed!')

  def disable_selinux(self):

    self.command('setenforce 0')
    selinux_dir = '/etc/selinux/config'
    file = open(selinux_dir,'r')
    list_of_lines = file.readlines()
    list_of_lines[6] = 'SELINUX=permissive\n'

    file = open(selinux_dir,'w')
    file.writelines(list_of_lines)
    file.close()

    self.command('sestatus')

    print('\n-> SELinux disabled!')

  def selinux_log(self):

    selinux_dir = '/etc/selinux/config'

    cmd = '-> SELinux:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {selinux_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")
    cmd = 'sestatus >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def disable_firewall(self):

    self.command('systemctl stop firewalld')
    self.command('systemctl disable firewalld')
    self.command('systemctl status firewalld --no-pager')

    print('\n-> Firewall disabled!')

  def firewall_log(self):

    cmd = '-> Firewall:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = 'systemctl status firewalld --no-pager >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def dirmaker(self):

    self.command('mkdir -p /tftp/{boot,grub}')
    self.command('mkdir -p /var/www/html/{desktop,server}')
    self.command('mkdir /var/www/html/server/centos8')
    # self.command('mkdir /var/www/html/desktop/focal')
    self.command('mkdir /tftp/boot/{windows10,centos8,focal}')
    self.command('mkdir /smbshare')
    self.command('mkdir /nfsshare')
    self.command('mkdir -p /netboot/files')
    self.command('mkdir /tftp/pxelinux.cfg')

    print('\n-> Created directories!')

  def dirmaker_log(self):

    cmd = '-> Directorys:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    boot_dir = '/tftp/boot'
    cmd = f'boot dir: {os.path.isdir(boot_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    grub_dir = '/tftp/grub'
    cmd = f'grub dir: {os.path.isdir(grub_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    desktop_dir = '/var/www/html/desktop'
    cmd = f'desktop dir: {os.path.isdir(desktop_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    server_dir = '/var/www/html/server'
    cmd = f'server dir: {os.path.isdir(server_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_windows_dir = '/tftp/boot/windows10'
    cmd = f'windows dir: {os.path.isdir(local_windows_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_ubuntu_dir = '/tftp/boot/focal'
    cmd = f'ubuntu dir: {os.path.isdir(local_ubuntu_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_centos_dir = '/tftp/boot/centos8'
    cmd = f'centos dir: {os.path.isdir(local_centos_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    smb_dir = '/smbshare'
    cmd = f'smb dir: {os.path.isdir(smb_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    nfs_dir = '/nfsshare'
    cmd = f'nfs dir: {os.path.isdir(nfs_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    netboot_dir = '/netboot/files'
    cmd = f'netboot files dir: {os.path.isdir(netboot_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    pxelinux_dir = '/netboot/files'
    cmd = f'pxelinux dir: {os.path.isdir(pxelinux_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    self.command("echo -e '\n' >> log.txt")

  def nfs_server_config(self):
    self.command('systemctl start nfs-server')
    self.command('systemctl enable nfs-server')
    self.command('systemctl status nfs-server --no-page')
    self.command('chown nobody:nobody /nfsshare')
    self.command('echo "/nfsshare *(ro)" >> /etc/exports')
    self.command('exportfs -r')

    print('\n--> Nfs-server configured!')

  def nfs_log(self):
    cmd = '-> NFS:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = 'systemctl status nfs-server --no-page >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def samba_config(self):

    self.command('useradd -s /sbin/nologin win10')
    self.command('echo -ne "123\n123\n" | smbpasswd -a -s win10')

    text = '\n[install]\n         comment = Installation Media\n         path = /smbshare\n         public = yes\n         writable = yes\n         printable = no\n         browseable = yes\n'

    dir_smb_config = '/etc/samba/smb.conf'

    file = open(dir_smb_config, 'a')
    file.write(text)
    file.close()

    self.command('systemctl start smb nmb')
    self.command('systemctl enable smb nmb')
    self.command('systemctl status smb nmb --no-page')

    print('\n--> Samba configured!')

  def samba_log(self):

    samba_dir = '/etc/samba/smb.conf'

    cmd = '-> Samba:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {samba_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")
    cmd = 'systemctl status smb nmb --no-page >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def dnsmasq_config(self):

    ips = self.range.split(',')
    ip_low = ips[0]
    ip_up = ips[1]

    text = f'interface={self.interface}\nbind-interfaces\ndomain=linuxhint.local\ndhcp-range={self.interface},{ip_low},{ip_up},{self.mask},8h\ndhcp-option=option:router,{self.gateway}\ndhcp-option=option:dns-server,{self.gateway}\ndhcp-option=option:dns-server,8.8.8.8\nenable-tftp\ntftp-root=/tftp\ndhcp-boot=pxelinux.0,linuxhint-s80,{self.local_ip}\npxe-prompt="Press F8 for PXE Network boot.",0\npxe-service=x86PC,"Install OS via PXE",pxelinux'

    if not os.path.isfile('/etc/dnsmasq.conf.backup'):
      self.command('mv -v /etc/dnsmasq.conf /etc/dnsmasq.conf.backup')

    dir_dnsmasq_config = '/etc/dnsmasq.conf'

    touch_cmd = f'touch {dir_dnsmasq_config}'

    self.command(touch_cmd)

    file = open(dir_dnsmasq_config,'w')
    file.write(text)
    file.close()

    self.command('systemctl restart dnsmasq')
    self.command('systemctl status dnsmasq --no-pager')
    self.command('systemctl enable dnsmasq')

    print('\n-> Dnsmasq configured!')

  def dnsmasq_log(self):

    dnsmasq_dir = '/etc/dnsmasq.conf'

    cmd = '-> Dnsmasq:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {dnsmasq_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")
    cmd = 'systemctl status dnsmasq --no-pager >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def apache_config(self):

    self.command('systemctl start httpd')
    self.command('systemctl status httpd --no-pager')
    self.command('systemctl enable http')

    print('\n-> Apache server configured!')

  def apache_log(self):

    cmd = '-> Apache:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = 'systemctl status httpd --no-pager >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def install_centos(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing centos: ')

      if inp == 'r' or inp == 'ready':
        dir_flag = os.path.isdir(self.centos_dir)

        if dir_flag:
          ready = True
        else:
          print('CentOS dir not found')

    if self.centos_dir_flag:

      strg = ''

      if self.centos_dir[0] == '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz {self.centos_dir} /var/www/html/server/centos8'
      elif self.centos_dir[0] == '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz {self.centos_dir}/ /var/www/html/server/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] == '/':
        strg = f'rsync -avz /{self.centos_dir} /var/www/html/server/centos8'
      elif self.centos_dir[0] != '/' and self.centos_dir[-1] != '/':
        strg = f'rsync -avz /{self.centos_dir}/ /var/www/html/server/centos8'

      self.command(strg)
      self.command('cp -v /var/www/html/server/centos8/images/pxeboot/{initrd.img,vmlinuz} /tftp/boot/centos8/')

    print('\n-> CentOS installed!')

  def install_windows(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing windows: ')

      if inp == 'r' or inp == 'ready':
        dir_flag = os.path.isdir(self.windows_dir)

        if dir_flag:
          ready = True
        else:
          print('Windows dir not found')

    if self.windows_dir_flag:

      strg = ''

      if self.windows_dir[0] == '/' and self.windows_dir[-1] == '/':
        strg = f'rsync -avz {self.windows_dir} /smbshare/windows10'
      elif self.windows_dir[0] == '/' and self.windows_dir[-1] != '/':
        strg = f'rsync -avz {self.windows_dir}/ /smbshare/windows10'
      elif self.windows_dir[0] != '/' and self.windows_dir[-1] == '/':
        strg = f'rsync -avz /{self.windows_dir} /smbshare/windows10'
      elif self.windows_dir[0] != '/' and self.windows_dir[-1] != '/':
        strg = f'rsync -avz /{self.windows_dir}/ /smbshare/windows10'

      self.command(strg)
      self.command('gdown --id 1xUWHsp4p0OJk-lGs3VhO6RM9iCc69RAf -O /smbshare/windows10/autounattend.xml')
      self.command('chown -R win10:win10 /smbshare/')
      self.command('chmod 777 -R /smbshare')
      self.command('gdown --id 1a1T7bSLXnB-yL1y7VvCzoPSANZKvW0lD -O /tftp/boot/windows10/winpe.iso')

      print('\n-> Windows installed!')

  def install_ubuntu(self):

    ready = False

    while not ready:
      inp = input('"r" or "ready" to continue installing ubuntu: ')

      if inp == 'r' or inp == 'ready':
        new_dir = self.ubuntu_dir.replace('\\','')
        dir_flag = os.path.isdir(new_dir)

        if dir_flag:
          ready = True
        else:
          print('Ubuntu dir not found')

    if self.ubuntu_dir_flag:

      strg = ''

      if self.ubuntu_dir[0] == '/' and self.ubuntu_dir[-1] == '/':
        strg = f'rsync -avz {self.ubuntu_dir} /nfsshare/focal'
      elif self.ubuntu_dir[0] == '/' and self.ubuntu_dir[-1] != '/':
        strg = f'rsync -avz {self.ubuntu_dir}/ /nfsshare/focal'
      elif self.ubuntu_dir[0] != '/' and self.ubuntu_dir[-1] == '/':
        strg = f'rsync -avz /{self.ubuntu_dir} /nfsshare/focal'
      elif self.ubuntu_dir[0] != '/' and self.ubuntu_dir[-1] != '/':
        strg = f'rsync -avz /{self.ubuntu_dir}/ /nfsshare/focal'

      self.command(strg)

      text = f'# Enable extras.ubuntu.com.\nd-i     apt-setup/extras        boolean true\n# Install the Ubuntu desktop.\ntasksel tasksel/first   multiselect ubuntu-desktop\n# On live DVDs, dont spend huge amounts of time removing substantial\n# application packages pulled in by language packs. Given that we clearly\n# have the space to include them on the DVD, theyre useful and we might as\n# well keep them installed.\nubiquity        ubiquity/keep-installed string icedtea6-plugin openoffice.org\n#System language\nlang en_US\n#Language modules to install\nlangsupport en_US\n#System keyboard\nkeyboard us\n#System mouse\nmouse\n#System timezone\ntimezone America/Sao_Paulo\n#Root password\nrootpw --disabled\n#Initial user (user with sudo capabilities)\nuser novousuario --fullname "Usuario" --password novo1234\n#Reboot after installation\nreboot\n#Use text mode install\ntext\n#Install OS instead of upgrade\ninstall\n#Installation media\nnfs --server={self.local_ip} --dir=/nfsshare/focal/\n#System bootloader configuration\nbootloader --location=mbr\n#Clear the Master Boot Record\nzerombr yes\n#Partition clearing information\nclearpart --all --initlabel\n#Basic disk partition\npart / --fstype ext4 --size 1 --grow --asprimary\npart swap --size 1024\npart /boot --fstype ext4 --size 256 --asprimary\n#System authorization infomation\nauth  --useshadow  --enablemd5\n#Network information\nnetwork --bootproto=dhcp --device={self.interface}\n#Firewall configuration\nfirewall --disabled --trust={self.interface} --ssh\n\n%packages\nubuntu-desktop\n'

      ubuntu_ks_dir = '/nfsshare/focal/preseed/ubuntu.seed'

      # if not os.path.isfile(ubuntu_ks_dir):
      #   touch_ubuntu_ks_dir = f'touch {ubuntu_ks_dir}'
      #   self.command(touch_ubuntu_ks_dir)

      file = open(ubuntu_ks_dir, 'w')
      file.write(text)
      file.close()

      self.command('cd /netboot')
      self.command('wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/netboot.tar.gz')
      self.command('tar xvf netboot.tar.gz -C /netboot/files')
      self.command('rm -r netboot.tar.gz')
      self.command('cd')
      self.command('cp -v /netboot/files/ubuntu-installer/amd64/{linux,initrd.gz} /tftp/boot/focal/')

      print('\n-> Ubuntu installed!')

  def syslinux_config(self):

    self.command('cp -v /usr/share/syslinux/{pxelinux.0,memdisk,menu.c32,ldlinux.c32,libutil.c32,vesamenu.c32,lpxelinux.0,libcom32.c32} /tftp/')

    print('\n-> Syslinux configured!')

  def install_menu(self):

    default_menu_dir = '/tftp/pxelinux.cfg/default'

    if not os.path.isfile(default_menu_dir):
      touch_default_menu = f'touch {default_menu_dir}'
      self.command(touch_default_menu)

    else:
      cmd = f'rm -f {default_menu_dir}'
      self.command(cmd)
      touch_default_menu = f'touch {default_menu_dir}'
      self.command(touch_default_menu)

    if self.os_selected == 0:
      text = f'DEFAULT vesamenu.c32\nMENU TITLE ULTIMATE PXE SERVER - By Griffon - Ver 2.0\nPROMPT 0\nTIMEOUT 0\n\nMENU COLOR TABMSG  37;40  #ffffffff #00000000\nMENU COLOR TITLE   37;40  #ffffffff #00000000\nMENU COLOR SEL      7     #ffffffff #00000000\nMENU COLOR UNSEL    37;40 #ffffffff #00000000\nMENU COLOR BORDER   37;40 #ffffffff #00000000\n\nLABEL Ubuntu 20\n    kernel /boot/focal/linux\n    initrd /boot/focal/initrd.gz\n    append netboot=nfs ip=dhcp ks=nfs:{self.local_ip}:/nfsshare/focal/preseed/ubuntu.seed --- quiet\n\nLABEL CentOS 8\n    kernel /boot/centos8/vmlinuz\n    initrd /boot/centos8/initrd.img\n    append ip=dhcp inst.repo=http://{self.local_ip}/server/centos8/\n\nLABEL Windows 10\n    kernel memdisk\n    initrd /tftp/boot/windows10/winpe.iso\n    append iso raw\n'
    else:
      text = f'DEFAULT {self.os_selected}\n\nLABEL 1\n    menu label ^Ubuntu 20\n    kernel /boot/focal/linux\n    initrd /boot/focal/initrd.gz\n    append netboot=nfs ip=dhcp ks=nfs:{self.local_ip}:/nfsshare/focal/preseed/ubuntu.seed --- quiet\n\nLABEL 2\n    menu label ^CentOS 8\n    kernel /boot/centos8/vmlinuz\n    initrd /boot/centos8/initrd.img\n    append ip=dhcp inst.repo=http://{self.local_ip}/server/centos8/\n\nLABEL 3\n    menu label ^Windows 10\n    kernel memdisk\n    initrd /tftp/boot/windows10/winpe.iso\n    append iso raw\n'

    file = open(default_menu_dir,'w')
    file.write(text)
    file.close()

    print('\n-> Menu configured!')

  def menu_log(self):

    default_menu_dir = '/tftp/pxelinux.cfg/default'

    cmd = '-> Menu:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {default_menu_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def status(self):

    self.command('systemctl status nfs-server smb nmb dnsmasq httpd --no-pager')

  def start(self):

    self.command('systemctl start nfs-server smb nmb dnsmasq httpd')

  def run(self, log=True):

    self.static_ip()
    self.install_stuff()
    self.disable_selinux()
    self.disable_firewall()
    self.dirmaker()
    self.nfs_server_config()
    self.samba_config()
    self.dnsmasq_config()
    self.apache_config()
    self.syslinux_config()
    self.install_menu()

    self.install_centos()
    self.install_windows()
    self.install_ubuntu()

    if log:
      self.logs()


  def logs(self):

    self.json_config_log()
    self.static_ip_log()
    self.selinux_log()
    self.firewall_log()
    self.dirmaker_log()
    self.nfs_log()
    self.samba_log()
    self.dnsmasq_log()
    self.apache_log()
    self.menu_log()

if __name__ == "__main__":

  pxe = PXE()
  pxe.read_json()

  # Get full command-line arguments
  full_cmd_arguments = sys.argv

  # Keep all but the first
  argument_list = full_cmd_arguments[1:]

  short_options = "r:nls:o"
  long_options = ["run=", "nolog", "list","select=","os"]

  # print(argument_list)

  if len(argument_list)==0:
    pxe.run()
  else:
    try:
      arguments, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
      # Output error, and return with an error code
      print(str(err))
      sys.exit(2)

    run_value = ''
    nolog_value = False
    list_value = False

    # Evaluate given options
    for current_argument, current_value in arguments:
      if current_argument in ("-r", "--run"):
        run_value = current_value
      elif current_argument in ("-n", "--nolog"):
        nolog_value = True
      elif current_argument in ("-l", "--list"):
        list_value = True
      elif current_argument in ("-s", "--select"):
        if current_value == 'manual' or current_value == 'ubuntu' or current_value == 'centos' or current_value == 'windows':
          pxe.select_os(current_value)
          pxe.install_menu()
        else:
          print(current_value)
          print('OS not found!')
      elif current_argument in ("-o", "--os"):
        print(self.os_selected)

    if not(run_value == ''):
      getattr(pxe, run_value)()

    if nolog_value:
      pxe.run(False)

    if list_value:
      print(str(dir(pxe)))

